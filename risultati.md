<script src='https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js?config=TeX-MML-AM_CHTML'></script>

# Risultati dei test dei vari modelli usando come dataset di riferimento Kitti Stereo 2015

## Analisi degli errori
### Metriche

- **abs rel**: absolute relative difference
\\[\frac{1}{|T|} \sum_{\substack{y \in T}} \frac{\|y -\hat{y}\| } {\hat{y}} \\] 
- **sq rel**: squared relative difference
\\[\frac{1}{|T|} \sum_{\substack{y \in T}}\frac{\|y -\hat{y}\|^2}{ \hat{y}}\\] 
- **rms**: root mean squared error
\\[\sqrt{\frac{1}{|T|} \sum_{\substack{y \in T}} \|y -\hat{y}\|^2}\\]
- **log rms**: log rms 
\\[\sqrt{\frac{1}{|T|} \sum_{\substack{y \in T}} \|\log{y} - \log{\hat{y}}\|^2}\\]
- **a1**:
- **a2**:
- **a3**:

![#c5f015](https://placehold.it/15/c5f015/000000?text=+) `Lower is better` 
![#1589F0](https://placehold.it/15/1589F0/000000?text=+) `Higher is better`

| Modello|Epoche|Batch size|# Immagini|<p style='color:#c5f015'>abs rel</p>|<p style='color:#c5f015'>sq rel</p>|<p style='color:#c5f015'>rms</p>|<p style='color:#c5f015'>log rms</p>|<p style='color:#c5f015'>d1 all</p>|<p style='color:#1589F0'>a1</p>|<p style='color:#1589F0'>a2</p>|<p style='color:#1589F0'>a3</p>|peso  collisioni|delta| 
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|First Train|30|8| 19735|0.1029|1.2881|5.396|0.186|21.016|0.891|0.956|0.981|-|-|
|Double SSIM clw0|30|6| 19735|0.1021|1.3761|5.362|0.184|20.256|0.897|0.959|0.982|0|-|
|Double SSIM clw03|30|6| 19735|0.1180|1.7695|5.779|0.206|21.989|0.884|0.950|0.977|0.3|0.5|
|clw02|30|6| 19735|0.1163|1.6466|5.635|0.203|22.280|0.882|0.950|0.976|0.2|0.5|
|clw03|30|6| 19735|0.1158|1.5724|5.633|0.201|22.186|0.881|0.952|0.978|0.3|0.5|
|clw05|30|6| 19735|0.1207|1.8670|5.746|0.209|23.267|0.873|0.945|0.976|0.5|0.5|


# Analisi del numero di collisioni
- Con `pixel esclusi` si intende tutti quei pixel che, a causa della disparita', vengono riproiettati fuori dall'immagine. Questi pixel non collidono, ma di fatto sono pixel non visibili nell'altra immagine.
- Con `pixel validi` si intende la differenza tra il numero di pixel totali nell'immagine tolti i `pixel esclusi`. L'insieme dei pixel che collidono con altri pixel è sottoinsieme dei `pixel validi`.
- Il numero di `pixel totali` è 131072 in quanto sono state utilizzate le disparità prodotte dalla rete, le quali hanno dimensione 512x256 e non 1242x375 come le immagini di Kitti 2015.

| Modello|# Medio di Collisioni|# Medio Pixel Esclusi|# Medio Pixel Validi|# Pixel Totali|% Esclusi sui Totali|% Validi sui Totali|% Collisioni sui Validi|% Collisioni sui Totali|
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|First Train|10705.345|1971.440| 129100.562|131072.000|1.504|98.496|8.292|8.168|
|Double SSIM clw0|5270.040|1105.040|129966.961|131072.000|0.843| 99.157|4.055|4.021|
|Double SSIM clw03|3648.235|1274.430|129797.570|131072.000|0.972|99.028|2.811|2.783|
|clw02|4596.030|1526.120|129545.883|131072.000|1.164|98.836|3.548|3.506|
|clw03|3732.795|1372.125|129699.883|131072.000|1.047|98.953|2.878|2.848|
|clw05|2472.500|1225.140|129846.859|131072.000|0.935|99.065|1.904|1.886|

<canvas id="myChart" width="200" height="100"></canvas>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.js" charset="utf-8"></script>
<script>
var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'horizontalBar',
    data: {
        labels: ["First Train", "clw02", "clw03", "clw05", "Double SSIM clw0", "Double SSIM clw03"],
        datasets: [{
            label: '# medio collisioni ',
            data: [10705, 4596, 3732, 2472, 5270, 3648 ],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
         title: {
            display: true,
            text: 'Numero Medio Collisioni'
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
        
    }
});
</script>

# Elaborazione di una immagine campione
Si consideri l'immagine 000042_10 appartenente a Kitti 2015. 
In seguito verranno mostrate le mappe di disparità prodotte dai vari modelli della rete quando l'ingresso è tale immagine. 

  <img src="https://gitlab.com/filippoaleotti/Risultati_Monodepth/raw/c4013c485f9e4334e3424b7a03a7a22678d0a2a7/imgs/000042_10.png" alt="000042_10">

## First Train

### Mappa disparità
<img src="https://gitlab.com/filippoaleotti/Risultati_Monodepth/raw/master/imgs/000042_10_disp_first_train.png" alt="disp">

### Collisioni
<img src="https://gitlab.com/filippoaleotti/Risultati_Monodepth/raw/master/imgs/000042_10_first_train_collision.jpg" alt="coll">

## Clw02 
<img src="https://gitlab.com/filippoaleotti/Risultati_Monodepth/raw/master/imgs/000042_10_disp_clw02.png" alt="disp">

### Collisioni
<img src="https://gitlab.com/filippoaleotti/Risultati_Monodepth/raw/master/imgs/000042_10_clw02_collision.jpg" alt="coll">

## Clw03
<img src="https://gitlab.com/filippoaleotti/Risultati_Monodepth/raw/master/imgs/000042_10_clw03_disp.png" alt="disp">

### Collisioni
<img src="https://gitlab.com/filippoaleotti/Risultati_Monodepth/raw/master/imgs/000042_10_clw03_collision.jpg" alt="coll">

## Clw05
<img src="https://gitlab.com/filippoaleotti/Risultati_Monodepth/raw/master/imgs/000042_10_clw05_disp.png" alt="disp">

### Collisioni
<img src="https://gitlab.com/filippoaleotti/Risultati_Monodepth/raw/master/imgs/000042_10_clw05_collision.jpg" alt="coll">

## Double SSIM clw0
<img src="https://gitlab.com/filippoaleotti/Risultati_Monodepth/raw/master/imgs/000042_10_double_ssim0_disp.png" alt="disp">

### Collisioni
<img src="https://gitlab.com/filippoaleotti/Risultati_Monodepth/raw/master/imgs/000042_10_double_ssim0_collision.jpg" alt="coll">

## Double SSIM clw03 
<img src="https://gitlab.com/filippoaleotti/Risultati_Monodepth/raw/master/imgs/000042_10_double_ssim03_disp.png" alt="disp">

### Collisioni
<img src="https://gitlab.com/filippoaleotti/Risultati_Monodepth/raw/master/imgs/000042_10_double_ssim03_collision.jpg" alt="coll">


